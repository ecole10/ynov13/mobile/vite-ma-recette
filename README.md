# Vite ma recette

## Requirements

- [node](https://nodejs.org/en/)

### Android

- [android sdk](https://developer.android.com/studio/releases/platform-tools)
- [run app on android](https://reactnative.dev/docs/running-on-device)

>>>
⚠️ SDK location not found. Define location with an ANDROID_SDK_ROOT environment variable or by setting the sdk.dir path in your project's local properties

Créer un fichier local.properties dans le dossier android
y renseigner le path vers le sdk android

On Windows
```bash 
sdk.dir=%APPDATA%\\Local\\Android\\sdk
```

On Mac
```bash 
sdk.dir = /Users/$USER/Library/Android/sdk
```

On Linux
```bash 
sdk.dir = /home/$USER/Android/Sdk
```
>>>

### Ios

- [run app on ios](https://reactnative.dev/docs/running-on-device)

## Install

- Copier le fichier .env en .env.dev

- Renseigner les variables 

- Installer les dépendances

```bash
yarn install
```

- Démarrer le serveur JS

```bash
yarn start
```

- Démarrer l'application sur mobile

### Android

```bash
yarn android
```

### Ios

```bash
yarn ios
```