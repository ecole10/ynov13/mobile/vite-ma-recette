export interface Ingredient {
  id: number;
  name: string;
  image: string;
  quantity?: string;
  unit?: string;
}
