import logo from '../../assets/img/logo.png';

export interface Ingredient {
  image: string;
  measures: {
    metric: {
      amount: number;
      unitShort: string;
    };
  };
  name: string; //produit au singulier
  nameClean: string; //produit au pluriel
}

export const ingredientPlaceholder: Ingredient = {
  image: '',
  measures: {
    metric: {
      unitShort: '',
      amount: 1,
    },
  },
  name: '',
  nameClean: '',
};

interface Step {
  number: number;
  step: string;
}

export interface RecipeDetails {
  analyzedInstructions: [{steps: [Step]}];
  diets: [string];
  extendedIngredients: Ingredient[];
  id: number;
  image: string;
  readyInMinutes: number;
  servings: number;
  spoonacularScore: number;
  summary: string;
  title: string;
}

export const recipeDetailsPlaceholder: RecipeDetails = {
  analyzedInstructions: [{steps: [{number: 1, step: ''}]}],
  diets: [''],
  extendedIngredients: [
    {
      image: logo,
      measures: {
        metric: {
          unitShort: '',
          amount: 1,
        },
      },
      name: '',
      nameClean: '',
    },
  ],
  id: 1,
  image: '',
  readyInMinutes: 0,
  servings: 1,
  spoonacularScore: 100,
  summary: '',
  title: '',
};
