import firebase from 'firebase/compat';
import {
  signInWithEmailAndPassword,
  signOut,
  getAuth,
  createUserWithEmailAndPassword,
} from 'firebase/auth';
import {Alert} from 'react-native';
import Config from 'react-native-config';
import {translate} from './utils/translation';
const firebaseConfig = {
  apiKey: Config.FIREBASE_API_KEY,
  authDomain: Config.FIREBASE_AUTH_DOMAIN,
  projectId: Config.FIREBASE_PROJECT_ID,
  storageBucket: Config.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: Config.FIREBASE_MESSAGING_SENDER_ID,
  appId: Config.FIREBASE_APP_ID,
  measurementId: Config.FIREBASE_MEASUREMENT_ID,
};

firebase.initializeApp(firebaseConfig);

export const signUserIn = async (login: string, password: string) => {
  return signInWithEmailAndPassword(getAuth(), login, password).catch(err => {
    console.error('Erreur firebase auth: ' + JSON.stringify(err));
    Alert.alert(
      translate('user.firebase.auth_failure'),
      translate('user.firebase.bad_identifier'),
    );
  });
};

export const signUserUp = async (email: string, password: string) => {
  await createUserWithEmailAndPassword(getAuth(), email, password)
    .then(() =>
      Alert.alert(
        translate('user.firebase.login_success'),
        translate('user.firebase.can_connect'),
      ),
    )
    .catch(err => {
      console.error('Erreur sign up firebase: ' + JSON.stringify(err));
      Alert.alert(
        translate('user.firebase.login_failure'),
        translate('user.firebase.bad_identifier'),
      );
    });
};
export const signUserOut = async () => signOut(getAuth());

export const getUser = async () => firebase.auth().currentUser;
