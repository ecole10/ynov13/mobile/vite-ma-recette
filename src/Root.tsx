import React, {useContext, useEffect} from 'react';
import Navigation from './navigations/Navigation';
import {UserContext} from './contexts/UserContext';
import UserStackNavigator from './navigations/UserStackNavigator';
import SplashScreen from 'react-native-splash-screen';

export default function Root() {
  const {checkUser, userid} = useContext(UserContext);

  useEffect(() => {
    const launch = async () => {
      await checkUser();
      SplashScreen.hide();
    };
    launch().then(() => {});
  }, []);

  // @ts-ignore
  return userid.length ? <Navigation /> : <UserStackNavigator />;
}
