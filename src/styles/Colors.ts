const Colors = {
  primary: '#FF965B',
  secondary: '#FFB78C',
  accent: '#BFBFBF',
  white: '#FFF',
  black: '#000',
  red: '#ef2626',
  grey: '#7C7C7C',
  transparent: 'transparent',
};

export default Colors;
