import {StyleSheet} from 'react-native';
import Fonts from './Fonts';
import Colors from './Colors';

const GlobalStyles = StyleSheet.create({
  headerTitle: {
    fontFamily: Fonts.vibur,
    fontSize: 28,
  },
  titlePrimary: {
    fontFamily: Fonts.vibur,
    fontSize: 32,
    color: Colors.primary,
    marginBottom: 5,
  },
  shoppingListHeaderButton: {
    marginRight: 20,
    backgroundColor: Colors.primary,
    color: Colors.white,
  },
});

export default GlobalStyles;
