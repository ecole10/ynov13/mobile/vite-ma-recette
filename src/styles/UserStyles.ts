import {StyleSheet} from 'react-native';
import Fonts from './Fonts';
import Colors from './Colors';

const UserStyles = StyleSheet.create({
  container: {
    paddingHorizontal: 50,
    flexGrow: 1,
  },
  image: {
    alignSelf: 'center',
    width: 160,
    height: 150,
    marginTop: 50,
    marginBottom: 30,
  },
  title: {
    fontFamily: Fonts.vibur,
    fontSize: 32,
    color: Colors.primary,
  },
  input: {
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    color: Colors.black,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
    padding: 15,
    marginBottom: 20,
  },
  button: {
    height: 40,
    backgroundColor: Colors.primary,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    color: Colors.white,
  },
  buttomView: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 30,
    justifyContent: 'center',
  },
});

export default UserStyles;
