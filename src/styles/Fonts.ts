const Fonts = {
  vibur: 'Vibur-Regular',
  quicksand: 'Quicksand-Regular',
};

export default Fonts;
