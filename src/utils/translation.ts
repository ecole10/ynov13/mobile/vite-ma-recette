import * as RNLocalize from 'react-native-localize';
const i18n = require('i18n-js');
const memoize = require('lodash.memoize');

export enum Lang {
  FR,
  EN,
}

const translationGetters = {
  fr: () => require('../locales/fr.json'),
  en: () => require('../locales/en.json'),
};

export const translate = memoize(
  (key: string, config: any) => i18n.t(key, config),
  (key: string, config: any) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = () => {
  const fallback = {languageTag: 'en'};
  const {languageTag} =
    fallback ||
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters));
  translate.cache.clear();
  // @ts-ignore
  i18n.translations = {[languageTag]: translationGetters[languageTag]()};
  i18n.locale = languageTag;
};

export const needTranslation = (): boolean => {
  return i18n.locale.toUpperCase() === Lang[Lang.FR];
};
