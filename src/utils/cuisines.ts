import {translate} from './translation';
import {CuisinesType} from '../hooks/spoonacular/useRecipeByCategory';

export const cuisines = [
  {
    title: translate('cuisines.african'),
    id: CuisinesType.AFRICAN,
    image: require('../../assets/img/cuisine/african.jpg'),
  },
  {
    title: translate('cuisines.american'),
    id: CuisinesType.AMERICAN,
    image: require('../../assets/img/cuisine/american.jpg'),
  },
  {
    title: translate('cuisines.british'),
    id: CuisinesType.BRITISH,
    image: require('../../assets/img/cuisine/british.jpg'),
  },
  {
    title: translate('cuisines.cajun'),
    id: CuisinesType.CAJUN,
    image: require('../../assets/img/cuisine/cajun.jpg'),
  },
  {
    title: translate('cuisines.caribbean'),
    id: CuisinesType.CARIBBEAN,
    image: require('../../assets/img/cuisine/caribbean.jpg'),
  },
  {
    title: translate('cuisines.chinese'),
    id: CuisinesType.CHINESE,
    image: require('../../assets/img/cuisine/chinese.jpg'),
  },
  {
    title: translate('cuisines.eastern_european'),
    id: CuisinesType.EASTERN_EUROPEAN,
    image: require('../../assets/img/cuisine/eastern_european.jpg'),
  },
  {
    title: translate('cuisines.european'),
    id: CuisinesType.EUROPEAN,
    image: require('../../assets/img/cuisine/european.jpg'),
  },
  {
    title: translate('cuisines.french'),
    id: CuisinesType.FRENCH,
    image: require('../../assets/img/cuisine/french.jpg'),
  },
  {
    title: translate('cuisines.german'),
    id: CuisinesType.GERMAN,
    image: require('../../assets/img/cuisine/german.jpg'),
  },
  {
    title: translate('cuisines.greek'),
    id: CuisinesType.GREEK,
    image: require('../../assets/img/cuisine/greek.jpg'),
  },
  {
    title: translate('cuisines.indian'),
    id: CuisinesType.INDIAN,
    image: require('../../assets/img/cuisine/indian.jpg'),
  },
  {
    title: translate('cuisines.irish'),
    id: CuisinesType.IRISH,
    image: require('../../assets/img/cuisine/irish.jpg'),
  },
  {
    title: translate('cuisines.italian'),
    id: CuisinesType.ITALIAN,
    image: require('../../assets/img/cuisine/italian.jpg'),
  },
  {
    title: translate('cuisines.japanese'),
    id: CuisinesType.JAPANESE,
    image: require('../../assets/img/cuisine/japanese.jpg'),
  },
  {
    title: translate('cuisines.jewish'),
    id: CuisinesType.JEWISH,
    image: require('../../assets/img/cuisine/jewish.jpg'),
  },
  {
    title: translate('cuisines.korean'),
    id: CuisinesType.KOREAN,
    image: require('../../assets/img/cuisine/korean.jpg'),
  },
  {
    title: translate('cuisines.latin_american'),
    id: CuisinesType.LATIN_AMERICAN,
    image: require('../../assets/img/cuisine/latin_american.jpg'),
  },
  {
    title: translate('cuisines.mediterranean'),
    id: CuisinesType.MEDITERRANEAN,
    image: require('../../assets/img/cuisine/mediterranean.jpg'),
  },
  {
    title: translate('cuisines.mexican'),
    id: CuisinesType.MEXICAN,
    image: require('../../assets/img/cuisine/mexican.jpg'),
  },
  {
    title: translate('cuisines.middle_eastern'),
    id: CuisinesType.MIDDLE_EASTERN,
    image: require('../../assets/img/cuisine/middle_eastern.jpg'),
  },
  {
    title: translate('cuisines.nordic'),
    id: CuisinesType.NORDIC,
    image: require('../../assets/img/cuisine/nordic.jpg'),
  },
  {
    title: translate('cuisines.southern'),
    id: CuisinesType.SOUTHERN,
    image: require('../../assets/img/cuisine/southern.jpg'),
  },
  {
    title: translate('cuisines.spanish'),
    id: CuisinesType.SPANISH,
    image: require('../../assets/img/cuisine/spanish.jpg'),
  },
  {
    title: translate('cuisines.thai'),
    id: CuisinesType.THAI,
    image: require('../../assets/img/cuisine/thai.jpg'),
  },
  {
    title: translate('cuisines.vietnamese'),
    id: CuisinesType.VIETNAMESE,
    image: require('../../assets/img/cuisine/vietnamese.jpg'),
  },
];
