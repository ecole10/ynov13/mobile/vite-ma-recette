import {translate} from './translation';

export const convertTime = (readyInMinutes: number) => {
  if (readyInMinutes < 60) {
    return readyInMinutes.toString() + ' ' + translate('recipe.minutes');
  }
  const hours = Math.floor(readyInMinutes / 60);
  const minutes = readyInMinutes - hours * 60;
  return hours.toString() + ' h ' + minutes + ' min ';
};
