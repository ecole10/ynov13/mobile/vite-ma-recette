import {useQuery} from 'react-query';
import Config from 'react-native-config';
import {getData} from '../../api/api';
import {getTranslatedText} from '../../api/deepl/translateText';
import {Recipe} from '../../models/Recipe';
import {needTranslation} from '../../utils/translation';

export const getRandomRecipes = async (): Promise<Recipe[]> => {
  const data = await getData(
    `${Config.SPOONACULAR_API_URL}/recipes/random?apiKey=${Config.SPOONACULAR_API_KEY}&number=30`,
  );

  if (data.recipes && needTranslation()) {
    for (const recipe of data.recipes) {
      recipe.title = await getTranslatedText(recipe.title);
    }
  }

  return data.recipes || [];
};

const useRandomRecipes = () => useQuery('randomRecipes', getRandomRecipes);

export default useRandomRecipes;
