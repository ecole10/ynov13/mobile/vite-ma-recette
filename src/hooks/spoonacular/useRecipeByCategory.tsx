import {useQuery} from 'react-query';
import Config from 'react-native-config';
import {getData} from '../../api/api';
import {getTranslatedText} from '../../api/deepl/translateText';
import {Recipe} from '../../models/Recipe';
import {needTranslation, translate} from '../../utils/translation';

export enum CuisinesType {
  AFRICAN = 'african',
  AMERICAN = 'american',
  BRITISH = 'british',
  CAJUN = 'cajun',
  CARIBBEAN = 'caribbean',
  CHINESE = 'chinese',
  EASTERN_EUROPEAN = 'eastern european',
  EUROPEAN = 'european',
  FRENCH = 'french',
  GERMAN = 'german',
  GREEK = 'greek',
  INDIAN = 'indian',
  IRISH = 'irish',
  ITALIAN = 'italian',
  JAPANESE = 'japanese',
  JEWISH = 'jewish',
  KOREAN = 'korean',
  LATIN_AMERICAN = 'latin american',
  MEDITERRANEAN = 'mediterranean',
  MEXICAN = 'mexican',
  MIDDLE_EASTERN = 'middle eastern',
  NORDIC = 'nordic',
  SOUTHERN = 'southern',
  SPANISH = 'spanish',
  THAI = 'thai',
  VIETNAMESE = 'vietnamese',
}

const getRecipesByCategory = async (cuisine: string): Promise<Recipe[]> => {
  let url = `${Config.SPOONACULAR_API_URL}/recipes/complexSearch?apiKey=${Config.SPOONACULAR_API_KEY}&number=100`;

  if (Object.values(CuisinesType).includes(cuisine as CuisinesType)) {
    url += `&cuisine=${cuisine}`;
  } else {
    throw Error(translate('cuisines.errors.cuisine_not_found'));
  }

  const data = await getData(url);

  if (data.results && needTranslation()) {
    for (const recipe of data.results) {
      recipe.title = await getTranslatedText(recipe.title);
    }
  }

  return data.results || [];
};

const useRecipesByCategory = (cuisine: string) =>
  useQuery('cuisine', () => getRecipesByCategory(cuisine), {
    cacheTime: 0,
  });

export default useRecipesByCategory;
