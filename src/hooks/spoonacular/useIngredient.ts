import {useQuery} from 'react-query';
import Config from 'react-native-config';
import {getData} from '../../api/api';
import {getTranslatedText} from '../../api/deepl/translateText';
import {needTranslation} from '../../utils/translation';
import {Ingredient} from '../../models/Ingredient';

export const getIngredient = async (query: string): Promise<Ingredient> => {
  let url = `${Config.SPOONACULAR_API_URL}/food/ingredients/search?apiKey=${Config.SPOONACULAR_API_KEY}&number=1&query=${query}&`;

  const data = await getData(url);

  for (const ingredient of data.results) {
    if (needTranslation()) {
      ingredient.name = await getTranslatedText(ingredient.name);
    }
    ingredient.image = `${Config.SPOONACULAR_IMAGES_URL}/ingredients_100x100/${ingredient.image}`;
  }

  return data.results[0];
};

const useIngredient = (query: string) =>
  useQuery('ingredient', () => getIngredient(query), {
    cacheTime: 0,
  });

export default useIngredient;
