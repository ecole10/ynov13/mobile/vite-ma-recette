import {useQuery} from 'react-query';
import Config from 'react-native-config';
import {getData} from '../../api/api';
import {getTranslatedText} from '../../api/deepl/translateText';
import {Recipe} from '../../models/Recipe';
import {Lang, needTranslation} from '../../utils/translation';

export enum RecipesType {
  APPETIZER = 'appetizer',
  STARTER = 'side dish',
  MAIN_COURSE = 'main course',
  DESSERT = 'dessert',
}

const getRecipes = async (query: string): Promise<Recipe[]> => {
  let url = `${Config.SPOONACULAR_API_URL}/recipes/complexSearch?apiKey=${Config.SPOONACULAR_API_KEY}&number=100`;

  if (Object.values(RecipesType).includes(query as RecipesType)) {
    url += `&type=${query}`;
  } else {
    if (needTranslation()) {
      query = await getTranslatedText(query, Lang[Lang.EN]);
    }

    url += `&query=${query}`;
  }

  const data = await getData(url);

  if (data.results && needTranslation()) {
    for (const recipe of data.results) {
      recipe.title = await getTranslatedText(recipe.title);
    }
  }

  return data.results || [];
};

const useRecipes = (query: string) =>
  useQuery('recipes', () => getRecipes(query), {
    cacheTime: 0,
  });

export default useRecipes;
