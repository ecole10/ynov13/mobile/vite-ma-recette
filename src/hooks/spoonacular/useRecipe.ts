import {useQuery} from 'react-query';
import Config from 'react-native-config';
import {getData} from '../../api/api';
import {RecipeDetails} from '../../models/RecipeDetails';
import {needTranslation} from '../../utils/translation';
import {getTranslatedText} from '../../api/deepl/translateText';

const getRecipe = async (id: number): Promise<RecipeDetails> => {
  const data: RecipeDetails = await getData(
    `${Config.SPOONACULAR_API_URL}/recipes/${id}/information?apiKey=${Config.SPOONACULAR_API_KEY}`,
  );
  if (data && needTranslation()) {
    data.title = await getTranslatedText(data.title);
    data.summary = await getTranslatedText(data.summary);
    for (const step of data.analyzedInstructions[0].steps) {
      step.step = await getTranslatedText(step.step);
    }
    for (let diet of data.diets) {
      diet = await getTranslatedText(diet);
    }
    for (let ingredient of data.extendedIngredients) {
      ingredient.name = await getTranslatedText(ingredient.name);
      ingredient.nameClean = await getTranslatedText(ingredient.nameClean);
    }
  }
  return data;
};

const useRecipe = (id: number) =>
  useQuery('recipe', () => getRecipe(id), {cacheTime: 0});

export default useRecipe;
