import {useQuery} from 'react-query';
import Config from 'react-native-config';
import {getData} from '../../api/api';
import {getTranslatedText} from '../../api/deepl/translateText';
import {Recipe} from '../../models/Recipe';
import {Lang, needTranslation} from '../../utils/translation';

const getRecipesByIngredients = async (
  ingredients: string[],
): Promise<Recipe[]> => {
  if (needTranslation()) {
    for (let ingredient of ingredients) {
      ingredient = await getTranslatedText(ingredient, Lang[Lang.EN]);
    }
  }

  let url = `${Config.SPOONACULAR_API_URL}/recipes/findByIngredients?apiKey=${
    Config.SPOONACULAR_API_KEY
  }&number=100&ingredients=${ingredients.join()}`;
  const data = await getData(url);

  if (data && needTranslation()) {
    for (const recipe of data) {
      recipe.title = await getTranslatedText(recipe.title);
    }
  }

  return data || [];
};

const useRecipesByIngredients = (ingredients: string[]) =>
  useQuery('recipes', () => getRecipesByIngredients(ingredients), {
    cacheTime: 0,
  });

export default useRecipesByIngredients;
