import React from 'react';
import Icon from 'react-native-ionicons';
import GlobalStyles from '../styles/GlobalStyles';
import Colors from '../styles/Colors';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeStackNavigator from './HomeStackNavigator';
import FridgeStackNavigator from './FridgeStackNavigator';
import ProfileStackNavigator from './ProfileStackNavigator';
import ShoppingListStackNavigator from './ShoppingListStackNavigator';

export type RootTapParamList = {
  HomeTap: undefined;
  FridgeTap: undefined;
  ProfileTap: undefined;
  ShoppingListTap: undefined;
};

const RootTab = createBottomTabNavigator<RootTapParamList>();

export const headerOptions = {
  headerStyle: {
    backgroundColor: Colors.primary,
  },
  headerTintColor: Colors.white,
  headerTitleStyle: GlobalStyles.headerTitle,
};

const Navigation: React.FC<RootTapParamList> = () => (
  <NavigationContainer>
    <RootTab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          color = focused ? Colors.primary : Colors.accent;
          size = 35;

          switch (route.name) {
            case 'HomeTap':
              iconName = 'search';
              break;
            case 'FridgeTap':
              iconName = 'snow';
              break;
            case 'ProfileTap':
              iconName = 'person';
              break;
            case 'ShoppingListTap':
              iconName = 'list-box';
              break;
            default:
              throw new Error(`route (${route.name}) does not exist`);
          }

          return <Icon name={iconName} size={size} color={color} />;
        },
        tabBarShowLabel: false,
        tabBarHideOnKeyboard: true,
        headerShown: false,
      })}>
      <RootTab.Screen name="HomeTap" component={HomeStackNavigator} />
      <RootTab.Screen name="FridgeTap" component={FridgeStackNavigator} />
      <RootTab.Screen name="ProfileTap" component={ProfileStackNavigator} />
      <RootTab.Screen
        name="ShoppingListTap"
        component={ShoppingListStackNavigator}
      />
    </RootTab.Navigator>
  </NavigationContainer>
);

export default Navigation;
