import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {headerOptions} from './Navigation';
import {translate} from '../utils/translation';
import {Fridge} from '../screens/fridge/Fridge';
import {RecipesIngredients} from '../screens/fridge/RecipesIngredients';
import {Recipe} from '../screens/Recipe';

export type FridgeStackParamList = {
  Fridge: undefined;
  RecipesIngredients: {ingredients: string[]; headerTitle: string};
  Recipe: {headerTitle: string; id: number};
};

const RootStack = createStackNavigator<FridgeStackParamList>();

const FridgeStackNavigator = () => (
  <RootStack.Navigator screenOptions={headerOptions}>
    <RootStack.Screen
      name="Fridge"
      options={{
        headerTitle: translate('header.fridge'),
      }}
      component={Fridge}
    />
    <RootStack.Screen
      name="RecipesIngredients"
      options={props => {
        return {
          headerTitle: props.route.params.headerTitle,
        };
      }}
      component={RecipesIngredients}
    />
    <RootStack.Screen
      name="Recipe"
      options={props => {
        return {
          headerTitle: props.route.params.headerTitle,
        };
      }}
      component={Recipe}
    />
  </RootStack.Navigator>
);

export default FridgeStackNavigator;
