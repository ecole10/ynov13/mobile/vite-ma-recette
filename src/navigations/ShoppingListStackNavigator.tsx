import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {headerOptions} from './Navigation';
import {translate} from '../utils/translation';
import {ShoppingList} from '../screens/shoppingList/ShoppingList';
import {Ingredient} from '../models/Ingredient';
import Button from '../components/Button';
import GlobalStyles from '../styles/GlobalStyles';
import {ShoppingListEditor} from '../screens/shoppingList/ShoppingListEditor';

export type ShoppingListStackParamList = {
  ShoppingList: {ingredients: Ingredient[]};
  ShoppingListEditor: {ingredients?: Ingredient[]};
};

const RootStack = createStackNavigator<ShoppingListStackParamList>();

const ShoppingListStackNavigator = () => (
  <RootStack.Navigator screenOptions={headerOptions}>
    <RootStack.Screen
      name="ShoppingList"
      options={props => {
        return {
          headerTitle: translate('header.shopping_list'),
          headerRight: () => (
            <Button
              style={GlobalStyles.shoppingListHeaderButton}
              icon="add"
              onPress={() => props.navigation.navigate('ShoppingListEditor')}
            />
          ),
        };
      }}
      component={ShoppingList}
    />
    <RootStack.Screen
      name="ShoppingListEditor"
      options={{headerTitle: translate('header.shopping_list')}}
      component={ShoppingListEditor}
    />
  </RootStack.Navigator>
);

export default ShoppingListStackNavigator;
