import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {headerOptions} from './Navigation';
import {translate} from '../utils/translation';
import {Home} from '../screens/home/Home';
import {RecipesList} from '../screens/home/RecipesList';
import {RecipesCategories} from '../screens/home/RecipesCategories';
import {Recipe} from '../screens/Recipe';
import {RecipesCategory} from '../screens/home/RecipesCategory';

export type HomeStackParamList = {
  Home: undefined;
  RecipesList: {query: string; headerTitle: string};
  RecipesCategories: undefined;
  Recipe: {headerTitle: string; id: number};
  RecipesCategory: {headerTitle: string; id: string};
};

const RootStack = createStackNavigator<HomeStackParamList>();

const HomeStackNavigator = () => (
  <RootStack.Navigator screenOptions={headerOptions}>
    <RootStack.Screen
      name="Home"
      options={{
        headerTitle: translate('header.home'),
      }}
      component={Home}
    />
    <RootStack.Screen
      name="RecipesList"
      options={props => {
        return {
          headerTitle: props.route.params.headerTitle,
        };
      }}
      component={RecipesList}
    />
    <RootStack.Screen
      name="RecipesCategory"
      options={props => {
        return {
          headerTitle: props.route.params.headerTitle,
        };
      }}
      component={RecipesCategory}
    />
    <RootStack.Screen
      name="RecipesCategories"
      options={{
        headerTitle: translate('header.categories'),
      }}
      component={RecipesCategories}
    />
    <RootStack.Screen
      name="Recipe"
      options={props => {
        return {
          headerTitle: props.route.params.headerTitle,
        };
      }}
      component={Recipe}
    />
  </RootStack.Navigator>
);

export default HomeStackNavigator;
