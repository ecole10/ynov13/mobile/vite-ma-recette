import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {headerOptions} from './Navigation';
import {translate} from '../utils/translation';
import {Profile} from '../screens/profile/Profile';

export type ProfileStackParamList = {
  Profile: undefined;
};

const RootStack = createStackNavigator<ProfileStackParamList>();

const ProfileStackNavigator = () => (
  <RootStack.Navigator screenOptions={headerOptions}>
    <RootStack.Screen
      name="Profile"
      options={{
        headerTitle: translate('header.profile'),
      }}
      component={Profile}
    />
  </RootStack.Navigator>
);

export default ProfileStackNavigator;
