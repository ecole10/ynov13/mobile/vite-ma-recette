import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/auth/Login';
import SignUp from '../screens/auth/SignUp';
import React, {FC} from 'react';

export type UserRootStackParamList = {
  Login: undefined;
  SignUp: undefined;
};

const RootStack = createStackNavigator<UserRootStackParamList>();

const UserStackNavigator: FC<UserRootStackParamList> = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        initialRouteName={'Login'}
        screenOptions={{
          headerShown: false,
        }}>
        <RootStack.Screen name={'Login'} component={Login} />
        <RootStack.Screen name={'SignUp'} component={SignUp} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default UserStackNavigator;
