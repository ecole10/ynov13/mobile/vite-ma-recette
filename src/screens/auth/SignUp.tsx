import React, {useContext, useState} from 'react';
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../styles/Colors';
import {StackNavigationProp} from '@react-navigation/stack';
import {UserRootStackParamList} from '../../navigations/UserStackNavigator';
import {useNavigation} from '@react-navigation/native';
import {UserContext} from '../../contexts/UserContext';
import UserStyles from '../../styles/UserStyles';
import {translate} from '../../utils/translation';
import Button from '../../components/Button';
import Logo from '../../../assets/img/logo.png';

type authScreenProp = StackNavigationProp<UserRootStackParamList>;

const SignUp = () => {
  const navigation = useNavigation<authScreenProp>();
  const [pseudo, setPseudo] = useState('');
  const [email, setMail] = useState('');
  const [password, setPassword] = useState('');
  const {signUp} = useContext(UserContext);

  return (
    <ScrollView>
      <KeyboardAvoidingView
        behavior="padding"
        style={UserStyles.container}
        enabled>
        <Image source={Logo} style={UserStyles.image} />
        <Text style={UserStyles.title}>{translate('user.login.nickname')}</Text>
        <TextInput
          style={UserStyles.input}
          placeholder={'vitemonpseudo'}
          placeholderTextColor={Colors.accent}
          value={pseudo}
          onChangeText={txt => setPseudo(txt)}
        />
        <Text style={UserStyles.title}>{translate('user.login.email')}</Text>
        <TextInput
          style={UserStyles.input}
          placeholder={'vitemonmail@mail.com'}
          placeholderTextColor={Colors.accent}
          value={email}
          onChangeText={txt => setMail(txt)}
          keyboardType={'email-address'}
        />
        <Text style={UserStyles.title}>{translate('user.login.password')}</Text>
        <TextInput
          style={UserStyles.input}
          placeholder={'vitemonmotdepasse'}
          placeholderTextColor={Colors.accent}
          value={password}
          onChangeText={txt => setPassword(txt)}
          secureTextEntry
        />
        <Button
          style={UserStyles.button}
          onPress={() => {
            signUp(email, password).then(() => {
              navigation.navigate('Login');
            });
          }}
          title={translate('user.login.sign_up')}
        />
        <View style={UserStyles.buttomView}>
          <Text style={{color: Colors.black}}>
            {translate('user.login.already_account')}
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Login');
            }}>
            <Text style={{color: Colors.primary}}>
              {translate('user.login.click_here')}
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default SignUp;
