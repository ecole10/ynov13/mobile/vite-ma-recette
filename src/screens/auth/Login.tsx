import React, {useContext, useState} from 'react';
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {UserContext} from '../../contexts/UserContext';
import Colors from '../../styles/Colors';
import {StackNavigationProp} from '@react-navigation/stack';
import {UserRootStackParamList} from '../../navigations/UserStackNavigator';
import {useNavigation} from '@react-navigation/native';
import UserStyles from '../../styles/UserStyles';
import {translate} from '../../utils/translation';
import Button from '../../components/Button';
import Logo from '../../../assets/img/logo.png';

type authScreenProp = StackNavigationProp<UserRootStackParamList>;

const Login = () => {
  const navigation = useNavigation<authScreenProp>();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const {login} = useContext(UserContext);

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <KeyboardAvoidingView
        behavior="padding"
        style={UserStyles.container}
        enabled>
        <Image source={Logo} style={UserStyles.image} />

        <Text style={UserStyles.title}>{translate('user.login.email')}</Text>
        <TextInput
          style={UserStyles.input}
          placeholder={'vitemonmail@mail.com'}
          placeholderTextColor={Colors.accent}
          value={email}
          onChangeText={txt => setEmail(txt)}
          keyboardType={'email-address'}
        />

        <Text style={UserStyles.title}>{translate('user.login.password')}</Text>
        <TextInput
          style={UserStyles.input}
          placeholder={'vitemonmotdepasse'}
          placeholderTextColor={Colors.accent}
          value={password}
          onChangeText={txt => setPassword(txt)}
          secureTextEntry
        />

        <Button
          style={UserStyles.button}
          onPress={() => {
            login(email, password).then(() => {});
          }}
          title={translate('user.login.connect')}
        />

        <View style={UserStyles.buttomView}>
          <Text style={{color: Colors.black}}>
            {translate('user.login.not_sign_up')}
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('SignUp');
            }}>
            <Text style={{color: Colors.primary}}>
              {translate('user.login.click_here')}
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default Login;
