import React, {FC, useState} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {Alert, FlatList, Image, StyleSheet, Text, View} from 'react-native';
import SearchBar from '../../components/SearchBar';
import {translate} from '../../utils/translation';
import Colors from '../../styles/Colors';
import {FridgeStackParamList} from '../../navigations/FridgeStackNavigator';
import {getIngredient} from '../../hooks/spoonacular/useIngredient';
import {Ingredient} from '../../models/Ingredient';
import {Picker} from '@react-native-picker/picker';
import {TextInput} from 'react-native-paper';
import Button from '../../components/Button';
import Icon from 'react-native-ionicons';

type ScreenNavigationProp<T extends keyof FridgeStackParamList> =
  NativeStackNavigationProp<FridgeStackParamList, T>;

type ScreenRouteProp<T extends keyof FridgeStackParamList> = RouteProp<
  FridgeStackParamList,
  T
>;

type Props<T extends keyof FridgeStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const Fridge: FC<Props<'Fridge'>> = ({navigation}: Props<'Fridge'>) => {
  const [ingredientList, setIngredientList] = useState<Ingredient[]>([]);

  const addIngredients = (query: string) => {
    getIngredient(query)
      .then(ingredient => {
        if (!ingredient) {
          throw Error(query);
        }

        if (
          ingredientList.length === 0 ||
          ingredientList.find(item => item.id !== ingredient.id)
        ) {
          setIngredientList(list => [...list, ingredient]);
        }
      })
      .catch(err => {
        Alert.alert(
          translate(
            'components.ingredient_list.errors.ingredient_not_found.title',
          ),
          translate(
            'components.ingredient_list.errors.ingredient_not_found.message',
            {
              ingredient: err.message,
            },
          ),
        );
      });
  };

  const updateIngredient = (
    ingredient: Ingredient,
    propertyName: string,
    value: string,
  ) => {
    const newIngredientList = ingredientList.map(i => {
      if (i.id === ingredient.id) {
        // @ts-ignore
        i[propertyName] = value;
        return i;
      }
      return i;
    });

    setIngredientList(newIngredientList);
  };

  const removeIngredient = (ingredient: Ingredient) => {
    const newIngredientList = ingredientList.filter(
      i => i.id !== ingredient.id,
    );
    setIngredientList(newIngredientList);
  };

  return (
    <View style={styles.container}>
      <SearchBar
        phrases={translate('components.ingredient_list.search_placeholder')}
        icon="plus"
        onIconPress={(query: string) => addIngredients(query)}
      />

      <FlatList
        data={ingredientList}
        keyExtractor={(item: Ingredient, index) => `${item.id}`}
        renderItem={({item: ingredient}) => (
          <View style={styles.ingredientsContainer}>
            <View style={styles.ingredientContainer}>
              <Text style={styles.ingredientText}>{ingredient.name}</Text>
              <Image
                source={{
                  uri: ingredient.image,
                }}
                style={styles.ingredientImage}
              />
            </View>

            <View style={styles.ingredientContainer}>
              <TextInput
                value={ingredient.quantity}
                onChangeText={quantity =>
                  updateIngredient(ingredient, 'quantity', quantity)
                }
                activeUnderlineColor={Colors.primary}
                style={styles.input}
              />

              <Picker
                selectedValue={ingredient.unit}
                onValueChange={unit =>
                  updateIngredient(ingredient, 'unit', unit)
                }
                dropdownIconColor={Colors.primary}
                mode={'dropdown'}
                style={styles.picker}>
                <Picker.Item label="kg" value="kg" />
                <Picker.Item label="g" value="g" />
                <Picker.Item label="p" value="p" />
                <Picker.Item label="l" value="l" />
                <Picker.Item label="cl" value="cl" />
              </Picker>

              <Icon
                name="trash"
                size={25}
                color={Colors.red}
                onPress={() => removeIngredient(ingredient)}
              />
            </View>
          </View>
        )}
      />

      <View style={styles.searchRecipesContainer}>
        <Button
          style={
            ingredientList.length === 0
              ? styles.searchRecipesDisable
              : styles.searchRecipes
          }
          title={translate('fridge.search_recipes')}
          disabled={ingredientList.length === 0}
          onPress={() =>
            navigation.navigate('RecipesIngredients', {
              headerTitle: translate('fridge.recipes'),
              ingredients: ingredientList.map(i => i.name),
            })
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 20,
  },
  ingredientsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: Colors.accent,
    borderBottomWidth: 1,
    borderBottomColor: Colors.accent,
    marginBottom: 10,
  },
  ingredientContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ingredientText: {
    color: Colors.black,
    textAlignVertical: 'center',
    marginRight: 5,
    fontSize: 24,
  },
  ingredientImage: {
    height: 30,
    width: 30,
  },
  input: {
    margin: 10,
    height: 40,
    backgroundColor: Colors.white,
  },
  picker: {
    width: 100,
  },
  searchRecipesContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  searchRecipes: {
    backgroundColor: Colors.primary,
    height: 40,
    borderRadius: 20,
  },
  searchRecipesDisable: {
    backgroundColor: Colors.accent,
    height: 40,
    borderRadius: 20,
  },
});
