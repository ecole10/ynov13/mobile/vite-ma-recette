import React, {FC} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {StyleSheet, View} from 'react-native';
import CardList from '../../components/CardList';
import {Recipe} from '../../models/Recipe';
import {FridgeStackParamList} from '../../navigations/FridgeStackNavigator';
import useRecipesByIngredients from '../../hooks/spoonacular/useRecipesByIngredients';
import {Chip, Text} from 'react-native-paper';
import {translate} from '../../utils/translation';
import Fonts from '../../styles/Fonts';
import Colors from '../../styles/Colors';

type ScreenNavigationProp<T extends keyof FridgeStackParamList> =
  NativeStackNavigationProp<FridgeStackParamList, T>;

type ScreenRouteProp<T extends keyof FridgeStackParamList> = RouteProp<
  FridgeStackParamList,
  T
>;

type Props<T extends keyof FridgeStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const RecipesIngredients: FC<Props<'RecipesIngredients'>> = ({
  route,
  navigation,
}: Props<'RecipesIngredients'>) => {
  const {isLoading, data: recipes} = useRecipesByIngredients(
    route.params.ingredients,
  );

  return (
    <View style={styles.container}>
      <Text style={styles.ingredientsText}>
        {translate('fridge.recipes_ingredients')}
      </Text>

      <View style={styles.ingredientContainer}>
        {route.params.ingredients?.map((ingredient, index) => (
          <Chip
            key={`${ingredient}-${index}`}
            mode="outlined"
            style={styles.ingredientChip}
            textStyle={styles.ingredientChipText}>
            {ingredient}
          </Chip>
        ))}
      </View>

      <Text style={styles.ingredientsText}>
        {translate('fridge.recipes_suggest')}
      </Text>

      <CardList
        isLoading={isLoading}
        data={recipes}
        onPress={(item: Recipe) => {
          navigation.navigate('Recipe', {
            headerTitle: item.title,
            id: item.id,
          });
        }}
      />

      {!isLoading && recipes?.length === 0 && (
        <Text style={styles.noRecipesText}>
          {translate('fridge.no_recipes')}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  ingredientsText: {
    fontFamily: Fonts.vibur,
    fontSize: 32,
    color: Colors.primary,
  },
  ingredientContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    marginBottom: 15,
  },
  ingredientChip: {
    backgroundColor: Colors.secondary,
    width: 100,
    marginRight: 10,
    marginTop: 10,
  },
  ingredientChipText: {
    color: Colors.white,
    fontWeight: 'bold',
    textAlign: 'center',
    width: 70,
  },
  noRecipesText: {
    textAlign: 'center',
    marginTop: 20,
  },
});
