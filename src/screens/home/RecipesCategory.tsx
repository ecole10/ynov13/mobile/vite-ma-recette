import React, {FC} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {HomeStackParamList} from '../../navigations/HomeStackNavigator';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import CardList from '../../components/CardList';
import {Recipe} from '../../models/Recipe';
import useRecipesByCategory from '../../hooks/spoonacular/useRecipeByCategory';
import {cuisines} from '../../utils/cuisines';
import Fonts from '../../styles/Fonts';
import GlobalStyles from '../../styles/GlobalStyles';
import {translate} from '../../utils/translation';

type ScreenNavigationProp<T extends keyof HomeStackParamList> =
  NativeStackNavigationProp<HomeStackParamList, T>;

type ScreenRouteProp<T extends keyof HomeStackParamList> = RouteProp<
  HomeStackParamList,
  T
>;

type Props<T extends keyof HomeStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const RecipesCategory: FC<Props<'RecipesCategory'>> = ({
  route,
  navigation,
}: Props<'RecipesCategory'>) => {
  const {isLoading, data: recipes} = useRecipesByCategory(route.params.id);

  const category = cuisines.find(item => route.params.id === item.id);

  return (
    <View>
      <ImageBackground
        source={category?.image}
        resizeMode={'cover'}
        style={styles.img}>
        <View style={styles.darken} />
        <Text style={styles.text}>{category?.title.toUpperCase()}</Text>
      </ImageBackground>
      <View style={styles.container}>
        <Text style={GlobalStyles.titlePrimary}>
          {translate('home.recipes_suggest')}
        </Text>
        <CardList
          isLoading={isLoading}
          data={recipes}
          onPress={(item: Recipe) => {
            navigation.navigate('Recipe', {
              headerTitle: item.title,
              id: item.id,
            });
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  img: {
    height: 150,
    idth: '100%',
    justifyContent: 'center',
    position: 'relative',
  },
  text: {
    width: '100%',
    fontFamily: Fonts.quicksand,
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
  darken: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    backgroundColor: '#00000099',
  },
});
