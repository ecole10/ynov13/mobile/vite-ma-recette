import React, {FC} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {StyleSheet, Text, View} from 'react-native';
import useRandomRecipes, {
  getRandomRecipes,
} from '../../hooks/spoonacular/useRandomRecipes';
import {translate} from '../../utils/translation';
import Button from '../../components/Button';
import SearchBar from '../../components/SearchBar';
import LineButton from '../../components/LineButton';
import {HomeStackParamList} from '../../navigations/HomeStackNavigator';
import {RecipesType} from '../../hooks/spoonacular/useRecipes';
import CardList from '../../components/CardList';
import {Recipe} from '../../models/Recipe';
import GlobalStyles from '../../styles/GlobalStyles';

type ScreenNavigationProp<T extends keyof HomeStackParamList> =
  NativeStackNavigationProp<HomeStackParamList, T>;

type ScreenRouteProp<T extends keyof HomeStackParamList> = RouteProp<
  HomeStackParamList,
  T
>;

type Props<T extends keyof HomeStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const Home: FC<Props<'Home'>> = ({navigation}: Props<'Home'>) => {
  const {isLoading, data: recipes} = useRandomRecipes();

  return (
    <View style={styles.container}>
      <SearchBar
        phrases={translate('home.search_placeholder')}
        icon="magnify"
        onIconPress={(query: string) =>
          navigation.navigate('RecipesList', {
            headerTitle: query,
            query: query,
          })
        }
      />

      <View style={styles.filterContainer}>
        <Button
          style={styles.filter}
          title={translate('home.filters.appetizer')}
          onPress={() =>
            navigation.navigate('RecipesList', {
              headerTitle: translate('home.filters.appetizer'),
              query: RecipesType.APPETIZER,
            })
          }
        />
        <Button
          style={styles.filter}
          title={translate('home.filters.starter')}
          onPress={() =>
            navigation.navigate('RecipesList', {
              headerTitle: translate('home.filters.starter'),
              query: RecipesType.STARTER,
            })
          }
        />
        <Button
          style={styles.filter}
          title={translate('home.filters.main_course')}
          onPress={() =>
            navigation.navigate('RecipesList', {
              headerTitle: translate('home.filters.main_course'),
              query: RecipesType.MAIN_COURSE,
            })
          }
        />
        <Button
          style={styles.filter}
          title={translate('home.filters.dessert')}
          onPress={() =>
            navigation.navigate('RecipesList', {
              headerTitle: translate('home.filters.dessert'),
              query: RecipesType.DESSERT,
            })
          }
        />
      </View>

      <LineButton onPress={() => navigation.navigate('RecipesCategories')} />

      <Text style={GlobalStyles.titlePrimary}>
        {translate('home.recipes_suggest')}
      </Text>

      <CardList
        isLoading={isLoading}
        data={recipes}
        refresh={getRandomRecipes}
        onPress={(item: Recipe) => {
          navigation.navigate('Recipe', {
            headerTitle: item.title,
            id: item.id,
          });
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    marginBottom: 20,
  },
  filterContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    justifyContent: 'space-between',
    marginBottom: 100,
  },
  filter: {
    height: 40,
    width: 170,
    marginTop: 10,
    borderRadius: 20,
  },
});
