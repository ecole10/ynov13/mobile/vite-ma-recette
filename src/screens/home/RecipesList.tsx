import React, {FC} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {HomeStackParamList} from '../../navigations/HomeStackNavigator';
import useRecipes from '../../hooks/spoonacular/useRecipes';
import {StyleSheet, View} from 'react-native';
import CardList from '../../components/CardList';
import {Recipe} from '../../models/Recipe';

type ScreenNavigationProp<T extends keyof HomeStackParamList> =
  NativeStackNavigationProp<HomeStackParamList, T>;

type ScreenRouteProp<T extends keyof HomeStackParamList> = RouteProp<
  HomeStackParamList,
  T
>;

type Props<T extends keyof HomeStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const RecipesList: FC<Props<'RecipesList'>> = ({
  route,
  navigation,
}: Props<'RecipesList'>) => {
  const {isLoading, data: recipes} = useRecipes(route.params.query);
  return (
    <View style={styles.container}>
      <CardList
        isLoading={isLoading}
        data={recipes}
        onPress={(item: Recipe) => {
          navigation.navigate('Recipe', {
            headerTitle: item.title,
            id: item.id,
          });
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
});
