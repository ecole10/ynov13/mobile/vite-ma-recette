import React, {FC, useContext, useEffect, useState} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {UserContext} from '../../contexts/UserContext';
import {translate} from '../../utils/translation';
import {ProfileStackParamList} from '../../navigations/ProfileStackNavigator';
import Button from '../../components/Button';
import Colors from '../../styles/Colors';
import EditButton from '../../components/EditButton';
import UploadImage from '../../components/UploadImage';

type ScreenNavigationProp<T extends keyof ProfileStackParamList> =
  NativeStackNavigationProp<ProfileStackParamList, T>;

type ScreenRouteProp<T extends keyof ProfileStackParamList> = RouteProp<
  ProfileStackParamList,
  T
>;

type Props<T extends keyof ProfileStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const Profile: FC<Props<'Profile'>> = () => {
  const {
    logout,
    checkImage,
    pseudo,
    handlePseudo,
    checkPseudo,
    regime,
    handleRegime,
    checkRegime,
    allergies,
    handleAllergies,
    checkAllergies,
  } = useContext(UserContext);
  const [tmpPseudo, setTmpPseudo] = useState('');
  const [tmpRegime, setTmpRegime] = useState('');
  const [tmpAllergies, setTmpAllergies] = useState('');

  useEffect(() => {
    const launch = async () => {
      await checkImage();
      await checkPseudo();
      await checkRegime();
      await checkAllergies();
    };
    launch().then(() => {});
  }, []);

  return (
    <ScrollView>
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <UploadImage />
          </View>

          <View style={styles.titleContainer}>
            <Text style={styles.title}>{translate('user.profile.pseudo')}</Text>
            <EditButton
              onPress={() => {
                handlePseudo(tmpPseudo);
              }}
            />
          </View>
          <TextInput
            style={styles.input}
            defaultValue={pseudo ? pseudo : translate('user.profile.pseudo')}
            onChangeText={text => setTmpPseudo(text)}
          />

          <View style={styles.titleContainer}>
            <Text style={styles.title}>{translate('user.profile.regime')}</Text>
            <EditButton
              onPress={() => {
                handleRegime(tmpRegime);
              }}
            />
          </View>
          <TextInput
            style={styles.input}
            defaultValue={regime ? regime : translate('user.profile.regime')}
            onChangeText={text => setTmpRegime(text)}
          />

          <View style={styles.titleContainer}>
            <Text style={styles.title}>
              {translate('user.profile.allergies')}
            </Text>
            <EditButton
              onPress={() => {
                handleAllergies(tmpAllergies);
              }}
            />
          </View>
          <TextInput
            style={styles.input}
            defaultValue={
              allergies ? allergies : translate('user.profile.allergies')
            }
            onChangeText={text => setTmpAllergies(text)}
          />

          <View style={styles.buttonContainer}>
            <Button
              style={styles.button}
              onPress={() => {
                logout();
              }}
              title={translate('user.profile.disconnect')}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 20,
  },
  imageContainer: {
    marginBottom: 20,
  },
  titleContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 102,
  },
  title: {
    fontSize: 20,
    color: Colors.secondary,
    marginRight: 5,
  },
  input: {
    color: Colors.black,
    fontSize: 20,
    marginLeft: 100,
  },
  buttonContainer: {
    marginTop: 70,
  },
  button: {
    backgroundColor: Colors.primary,
    height: 40,
    borderRadius: 20,
  },
});
