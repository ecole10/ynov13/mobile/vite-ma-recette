import React, {FC, useEffect, useState} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {
  ActivityIndicator,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {HomeStackParamList} from '../navigations/HomeStackNavigator';
import useRecipe from '../hooks/spoonacular/useRecipe';
import Colors from '../styles/Colors';
import Icon from 'react-native-ionicons';
import QuantityHandler from '../components/QuantityHandler';
import {RecipeDetails, recipeDetailsPlaceholder} from '../models/RecipeDetails';
import IngredientList from '../components/RecipeIngredients';
import Fonts from '../styles/Fonts';
import GlobalStyles from '../styles/GlobalStyles';
import {translate} from '../utils/translation';
import logo from '../../assets/img/logo.png';
import {convertTime} from '../utils/utils';

type ScreenNavigationProp<T extends keyof HomeStackParamList> =
  NativeStackNavigationProp<HomeStackParamList, T>;

type ScreenRouteProp<T extends keyof HomeStackParamList> = RouteProp<
  HomeStackParamList,
  T
>;

type Props<T extends keyof HomeStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const Recipe: FC<Props<'Recipe'>> = ({route}: Props<'Recipe'>) => {
  const {isLoading, data} = useRecipe(route.params.id);
  const [recipe, updateRecipe]: [
    RecipeDetails,
    (recipe: RecipeDetails) => void,
  ] = useState(recipeDetailsPlaceholder);
  const [servings, updateServings]: [number, (servings: number) => void] =
    useState(2);

  useEffect(() => {
    if (data) {
      updateRecipe(data);
      updateServings(data.servings);
    }
  }, [data, isLoading]);

  const removeTag = (text: string) => {
    return text.replace(/<\/?[^>]+(>|$)/g, '');
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator color={Colors.primary} />
      ) : (
        <ScrollView style={{width: '100%', height: '100%'}}>
          <ImageBackground
            source={recipe.image ? {uri: recipe.image} : logo}
            style={{height: 200}}
            resizeMode={recipe.image ? 'cover' : 'contain'}
          />
          <View style={styles.recipeContainer}>
            <View style={styles.infos}>
              {recipe.diets && recipe.diets.length > 0 ? (
                <Text style={styles.diet}>
                  {recipe.diets.map(diet => (
                    <Text key={diet}>{'#' + diet.replace(' ', '') + ' '}</Text>
                  ))}
                </Text>
              ) : (
                <View />
              )}
              <View style={styles.markContainer}>
                <Text style={styles.mark}>
                  {Math.round(recipe.spoonacularScore / 20)}
                </Text>
                <Icon name={'star'} style={{color: 'white'}} size={25} />
              </View>
            </View>
            <Text style={GlobalStyles.titlePrimary}>{recipe.title}</Text>
            <Text style={styles.text}>{removeTag(recipe.summary)}</Text>
            <View style={styles.cookingInfos}>
              <Icon
                name={'stopwatch'}
                style={{color: Colors.secondary}}
                size={40}
              />
              <Text style={styles.cookingInfosText}>
                {translate('recipe.time') +
                  ' ' +
                  convertTime(recipe.readyInMinutes)}
              </Text>
            </View>
            <View
              style={[styles.cookingInfos, {justifyContent: 'space-between'}]}>
              <View style={styles.servings}>
                <Icon
                  name={'people'}
                  style={{color: Colors.secondary}}
                  size={40}
                />
                <Text style={[styles.cookingInfosText]}>
                  {translate('recipe.for') + ' ' + servings + ' '}
                  {servings > 1
                    ? translate('recipe.servings')
                    : translate('recipe.serving')}
                </Text>
              </View>
              <QuantityHandler
                step={1}
                value={servings}
                updateValue={(value: number) => updateServings(value)}
              />
            </View>
            <IngredientList
              ingredients={recipe.extendedIngredients}
              servings={servings}
              baseServings={recipe.servings}
            />
            {recipe.analyzedInstructions[0].steps.map(step => (
              <View key={step.number}>
                <Text style={[GlobalStyles.titlePrimary, {marginVertical: 20}]}>
                  {translate('recipe.step') + ' ' + step.number}
                </Text>
                <Text style={styles.text}>{step.step}</Text>
              </View>
            ))}
            <Text style={[GlobalStyles.titlePrimary, styles.wish]}>
              {translate('recipe.wish')}
            </Text>
          </View>
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  recipeContainer: {marginHorizontal: 15},
  infos: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  diet: {
    fontFamily: Fonts.vibur,
    fontSize: 18,
    color: Colors.secondary,
    width: '85%',
  },
  markContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.secondary,
    paddingVertical: 2,
    paddingHorizontal: 7,
    borderRadius: 30,
    width: 50,
    justifyContent: 'space-around',
  },
  mark: {
    fontFamily: Fonts.vibur,
    fontSize: 18,
    color: 'white',
    transform: [{translateY: 2}],
  },
  text: {
    fontFamily: Fonts.quicksand,
    fontSize: 18,
    color: Colors.grey,
    textAlign: 'justify',
    marginBottom: 20,
  },
  cookingInfos: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  cookingInfosText: {
    fontFamily: Fonts.quicksand,
    fontSize: 18,
    color: 'black',
    paddingLeft: 20,
  },
  servings: {flexDirection: 'row', alignItems: 'center'},
  wish: {
    paddingVertical: 30,
    textAlign: 'center',
  },
});
