import React, {FC, useEffect, useState} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {StyleSheet, View} from 'react-native';
import {ShoppingListStackParamList} from '../../navigations/ShoppingListStackNavigator';
import IngredientList from '../../components/IngredientList';
import Button from '../../components/Button';
import Colors from '../../styles/Colors';
import {Ingredient} from '../../models/Ingredient';
import {translate} from '../../utils/translation';

type ScreenNavigationProp<T extends keyof ShoppingListStackParamList> =
  NativeStackNavigationProp<ShoppingListStackParamList, T>;

type ScreenRouteProp<T extends keyof ShoppingListStackParamList> = RouteProp<
  ShoppingListStackParamList,
  T
>;

type Props<T extends keyof ShoppingListStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const ShoppingList: FC<Props<'ShoppingList'>> = ({
  route,
  navigation,
}: Props<'ShoppingList'>) => {
  const [ingredientList, setIngredientList] = useState<Ingredient[]>([]);

  useEffect(() => {
    if (route.params && route.params.ingredients) {
      setIngredientList(route.params.ingredients);
    }
  }, [route.params]);

  return (
    <View style={styles.container}>
      <IngredientList data={ingredientList} />

      <View style={styles.editShoppingListContainer}>
        <Button
          style={
            !route.params
              ? styles.editShoppingListDisable
              : styles.editShoppingList
          }
          title={translate('shopping_list.edit_shopping_list')}
          disabled={!route.params}
          onPress={() =>
            navigation.navigate('ShoppingListEditor', {
              ingredients: ingredientList,
            })
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 20,
  },
  editShoppingListContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  editShoppingList: {
    backgroundColor: Colors.primary,
    height: 40,
    borderRadius: 20,
  },
  editShoppingListDisable: {
    backgroundColor: Colors.accent,
    height: 40,
    borderRadius: 20,
  },
});
