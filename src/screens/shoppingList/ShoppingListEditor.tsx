import React, {FC, useEffect, useState} from 'react';
import {RouteProp} from '@react-navigation/core';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {StyleSheet, View} from 'react-native';
import {ShoppingListStackParamList} from '../../navigations/ShoppingListStackNavigator';
import Button from '../../components/Button';
import Colors from '../../styles/Colors';
import {Ingredient} from '../../models/Ingredient';
import IngredientList from '../../components/IngredientList';
import {translate} from '../../utils/translation';

type ScreenNavigationProp<T extends keyof ShoppingListStackParamList> =
  NativeStackNavigationProp<ShoppingListStackParamList, T>;

type ScreenRouteProp<T extends keyof ShoppingListStackParamList> = RouteProp<
  ShoppingListStackParamList,
  T
>;

type Props<T extends keyof ShoppingListStackParamList> = {
  route: ScreenRouteProp<T>;
  navigation: ScreenNavigationProp<T>;
};

export const ShoppingListEditor: FC<Props<'ShoppingListEditor'>> = ({
  route,
  navigation,
}: Props<'ShoppingListEditor'>) => {
  const [ingredientList, setIngredientList] = useState<Ingredient[]>([]);

  useEffect(() => {
    if (route.params && route.params.ingredients) {
      setIngredientList(route.params.ingredients);
    }
  }, [route.params]);

  return (
    <View style={styles.container}>
      <IngredientList
        data={ingredientList}
        parentList={setIngredientList}
        search={true}
      />

      <View style={styles.validateShoppingListContainer}>
        <Button
          style={
            ingredientList.length === 0
              ? styles.validateShoppingListDisable
              : styles.validateShoppingList
          }
          title={translate('shopping_list.validate_shopping_list')}
          disabled={ingredientList.length === 0}
          onPress={() =>
            navigation.navigate('ShoppingList', {
              ingredients: ingredientList,
            })
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 20,
  },
  validateShoppingListContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  validateShoppingList: {
    backgroundColor: Colors.primary,
    height: 40,
    borderRadius: 20,
  },
  validateShoppingListDisable: {
    backgroundColor: Colors.accent,
    height: 40,
    borderRadius: 20,
  },
});
