const axios = require('axios').default;

export const getData = async (url: string) => {
  return axios
    .get(url)
    .then((response: {data: any}) => response.data)
    .catch((error: any) => console.error(error));
};
