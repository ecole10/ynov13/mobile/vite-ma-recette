import Config from 'react-native-config';
import axios from 'axios';
const i18n = require('i18n-js');
const qs = require('qs');

export const getTranslatedText = async (
  text: string,
  langTo: string = i18n.locale.toUpperCase(),
): Promise<string> => {
  return axios
    .post(
      `${Config.DEEPL_API_URL}/v2/translate`,
      qs.stringify({
        auth_key: Config.DEEPL_API_KEY,
        text: text,
        target_lang: langTo,
      }),
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      },
    )
    .then((response: {data: any}) => response.data.translations[0].text)
    .catch((error: any) => console.error(error));
};
