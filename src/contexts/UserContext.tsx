import React, {createContext, useState} from 'react';
import {updateUser} from '../api/User';
import {getUser, signUserIn, signUserOut, signUserUp} from '../firebase';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {translate} from '../utils/translation';

const UserIdKey = 'USERID_KEY';
const UserProfilePngKey = 'PROFILEPATH_KEY';
const UserPseudoKey = 'PSEUDO_KEY';
const UserRegimeKey = 'REGIME_KEY';
const UserAllergieKey = 'ALLERGIE_KEY';

export const UserContext = createContext({
  user: {email: '', password: '', id: ''},
  userid: '',
  userImage: {imagePath: ''},
  handleUserImage: (imagePath: string) => {},
  checkImage: async () => {},
  checkUser: async () => {},
  login: async (email: string, password: string) => {},
  logout: () => {},
  modifyUser: (
    newUserProps: {email: string; password: string},
    sendToServer = true,
  ) => {},
  signUp: async (email: string, password: string) => {},
  pseudo: '',
  handlePseudo: (newPseudo: string) => {},
  checkPseudo: async () => {},
  regime: '',
  handleRegime: (newRegime: string) => {},
  checkRegime: async () => {},
  allergies: '',
  handleAllergies: (newRegime: string) => {},
  checkAllergies: async () => {},
});

// @ts-ignore
export const UserProvider = ({children}) => {
  const [userid, setUserid] = useState('');
  const [user, setUser] = useState({email: '', password: '', id: ''});
  const [userImage, setUserImage] = useState({imagePath: ''});
  const [pseudo, setPseudo] = useState('');
  const [regime, setRegime] = useState('');
  const [allergies, setAllergies] = useState('');

  const handleUserId = async (userId: string | null) => {
    if (userId) {
      setUserid(userId);
      const user = await getUser();
      if (user?.uid && user?.email) {
        setUser({id: user.uid, password: '******', email: user.email});
      }
    }
  };

  const handleUserImage = (imagePath: string | null) => {
    if (imagePath) {
      const newImage = {imagePath: imagePath};
      AsyncStorage.setItem(UserProfilePngKey, imagePath).then(() => {});
      setUserImage(newImage);
    }
  };

  const checkImage = async () => {
    const localImagePath = await AsyncStorage.getItem(UserProfilePngKey);
    handleUserImage(localImagePath);
  };

  const handlePseudo = (newPseudo: string | null) => {
    if (newPseudo) {
      setPseudo(newPseudo);
      AsyncStorage.setItem(UserPseudoKey, newPseudo).then(() => {});
    }
  };

  const checkPseudo = async () => {
    const localUserPseudo = await AsyncStorage.getItem(UserPseudoKey);
    handlePseudo(localUserPseudo);
  };

  const handleRegime = (newRegime: string | null) => {
    if (newRegime) {
      setRegime(newRegime);
      AsyncStorage.setItem(UserRegimeKey, newRegime).then(() => {});
    }
  };

  const checkRegime = async () => {
    const localUserRegime = await AsyncStorage.getItem(UserRegimeKey);
    handleRegime(localUserRegime);
  };

  const handleAllergies = (newAllergies: string | null) => {
    if (newAllergies) {
      setAllergies(newAllergies);
      AsyncStorage.setItem(UserAllergieKey, newAllergies).then(() => {});
    }
  };

  const checkAllergies = async () => {
    const localUserAllergies = await AsyncStorage.getItem(UserAllergieKey);
    handleAllergies(localUserAllergies);
  };

  const checkUser = async () => {
    const localId = await AsyncStorage.getItem(UserIdKey);
    await handleUserId(localId);
  };

  const login = async (email: string, password: string) => {
    const userCred = await signUserIn(email, password);
    if (userCred && userCred.user) {
      if (userCred.user.email) {
        const tmpUser = {
          id: userCred.user.uid,
          email: userCred.user.email,
          password: '******',
        };
        setUserid(userCred.user.uid);
        setUser(tmpUser);
        await AsyncStorage.setItem(UserIdKey, userCred.user.email);
      } else {
        throw translate('user.context.error_mail');
      }
    }
  };

  const signUp = async (email: string, password: string) => {
    await signUserUp(email, password);
  };

  const logout = () => {
    signUserOut().then(() => {});
    setUserid('');
    setUserImage({imagePath: ''});
    setPseudo('');
    setRegime('');
    setAllergies('');
    AsyncStorage.removeItem(UserIdKey).then(() => {});
    AsyncStorage.removeItem(UserProfilePngKey);
    AsyncStorage.removeItem(UserPseudoKey);
    AsyncStorage.removeItem(UserAllergieKey);
    AsyncStorage.removeItem(UserRegimeKey);
  };

  const modifyUser = (
    newUserProps: {email: string; password: string},
    sendToServer = true,
  ) => {
    const newUser = {...user, ...newUserProps};
    setUser(newUser);
    if (sendToServer) updateUser({id: user.id, ...newUserProps}).then(() => {});
  };

  return (
    <UserContext.Provider
      value={{
        user,
        userid,
        userImage,
        handleUserImage,
        checkImage,
        checkUser,
        login,
        logout,
        modifyUser,
        signUp,
        pseudo,
        handlePseudo,
        checkPseudo,
        regime,
        handleRegime,
        checkRegime,
        allergies,
        handleAllergies,
        checkAllergies,
      }}>
      {children}
    </UserContext.Provider>
  );
};
