import React, {useContext} from 'react';
import {Alert, Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import Colors from '../styles/Colors';
import {launchImageLibrary} from 'react-native-image-picker';
import NoPhoto from '../../assets/img/no_photo.png';
import {UserContext} from '../contexts/UserContext';

const UploadImage = () => {
  const {userImage, handleUserImage} = useContext(UserContext);
  const image = {
    height: 150,
    width: 150,
  };

  const addImage = async () => {
    await launchImageLibrary(
      {
        maxWidth: image.width,
        maxHeight: image.height,
        mediaType: 'photo',
        selectionLimit: 1,
      },
      response => {
        if (response.didCancel) return;

        switch (response.errorCode) {
          case 'camera_unavailable':
            Alert.alert('Camera unavailable on device');
            break;
          case 'permission':
            Alert.alert('Permission not satisfied');
            break;
          case 'others':
            Alert.alert('Error on getting image');
            break;
        }

        if (response.assets && response.assets[0].uri) {
          handleUserImage(response.assets[0].uri);
        }
      },
    );
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={addImage} style={styles.imageContainer}>
        {userImage && (
          <Image
            source={
              userImage.imagePath !== '' ? {uri: userImage.imagePath} : NoPhoto
            }
            style={styles.image}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

export default UploadImage;

const styles = StyleSheet.create({
  container: {
    height: 150,
    width: 150,
    borderRadius: 80,
    overflow: 'hidden',
    alignSelf: 'center',
    borderColor: Colors.primary,
    borderWidth: 2,
  },
  imageContainer: {
    alignSelf: 'center',
  },
  image: {width: 150, height: 150},
});
