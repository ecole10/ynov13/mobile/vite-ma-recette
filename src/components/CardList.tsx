import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  RefreshControl,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import Colors from '../styles/Colors';
import Card, {Item} from './Card';

type CardList = {
  isLoading: boolean;
  data?: Item[];
  refresh?: () => Promise<Item[]>;
  onPress: (item: Item | any) => any;
};

const CardList = ({isLoading, data, refresh, onPress}: CardList) => {
  const [list, setList] = useState<Item[]>([]);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    data ? setList(data) : setList([]);
  }, [data]);

  const onRefresh = () => {
    setRefreshing(true);

    refresh?.().then(response => {
      setList(response);
      setRefreshing(false);
    });
  };

  return (
    <ScrollView
      refreshControl={
        refresh && (
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            colors={[Colors.primary]}
            tintColor={Colors.primary}
          />
        )
      }>
      {isLoading ? (
        <ActivityIndicator color={Colors.primary} />
      ) : (
        <View style={styles.container}>
          {list?.map(item => (
            <Card item={item} onPress={onPress} key={`${item.id}`} />
          ))}
        </View>
      )}
    </ScrollView>
  );
};

export default CardList;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    justifyContent: 'space-between',
  },
});
