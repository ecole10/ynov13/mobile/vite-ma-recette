import React, {useEffect, useState} from 'react';
import {Alert, FlatList, View} from 'react-native';
import SearchBar from './SearchBar';
import {Ingredient as IngredientModel} from '../models/Ingredient';
import Ingredient, {UnitType} from './Ingredient';
import {getIngredient} from '../hooks/spoonacular/useIngredient';
import {translate} from '../utils/translation';

type IngredientList = {
  data?: IngredientModel[];
  parentList?: (list: IngredientModel[]) => any;
  search?: boolean;
};

const IngredientList = ({data, parentList, search = false}: IngredientList) => {
  const [ingredientList, setIngredientList] = useState<IngredientModel[]>([]);

  useEffect(() => {
    if (data) {
      setIngredientList(data);
    }
  });

  const addIngredients = (query: string) => {
    getIngredient(query)
      .then((ingredient: IngredientModel) => {
        if (!ingredient) {
          throw Error(query);
        }

        if (
          ingredientList.length === 0 ||
          ingredientList.find(item => item.id !== ingredient.id)
        ) {
          setIngredientList(list => [...list, ingredient]);
          if (parentList) {
            parentList([...ingredientList, ingredient]);
          }
        }
      })
      .catch((err: {message: string}) => {
        Alert.alert(
          translate(
            'components.ingredient_list.errors.ingredient_not_found.title',
          ),
          translate(
            'components.ingredient_list.errors.ingredient_not_found.message',
            {
              ingredient: err.message,
            },
          ),
        );
      });
  };

  const updateIngredient = (
    ingredient: IngredientModel,
    propertyName: string,
    value: string,
  ) => {
    const newIngredientList = ingredientList.map(i => {
      if (i.id === ingredient.id) {
        // @ts-ignore
        i[propertyName] = value;

        if (propertyName !== 'unit' && !i.unit) {
          i.unit = Object.keys(UnitType)[0].toLowerCase();
        }

        return i;
      }
      return i;
    });

    setIngredientList(newIngredientList);

    if (parentList) {
      parentList(newIngredientList);
    }
  };

  const removeIngredient = (ingredient: IngredientModel) => {
    const newIngredientList = ingredientList.filter(
      i => i.id !== ingredient.id,
    );
    setIngredientList(newIngredientList);

    if (parentList) {
      parentList(newIngredientList);
    }
  };

  return (
    <View>
      {search ? (
        <SearchBar
          phrases={translate('components.ingredient_list.search_placeholder')}
          icon="plus"
          onIconPress={(query: string) => addIngredients(query)}
        />
      ) : null}

      <FlatList
        data={ingredientList}
        keyExtractor={(item: IngredientModel) => `${item.id}`}
        renderItem={({item: ingredient}) => (
          <Ingredient
            ingredient={ingredient}
            updateIngredient={search ? updateIngredient : undefined}
            removeIngredient={search ? removeIngredient : undefined}
          />
        )}
      />
    </View>
  );
};

export default IngredientList;
