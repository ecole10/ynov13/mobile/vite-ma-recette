import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TextInput} from 'react-native-paper';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-ionicons';
import Colors from '../styles/Colors';
import {Ingredient as IngredientModel} from '../models/Ingredient';

export enum UnitType {
  KG = 'kg',
  G = 'g',
  P = 'p',
  L = 'l',
  CL = 'cl',
}

type Ingredient = {
  ingredient: IngredientModel;
  updateIngredient?: (
    ingredient: IngredientModel,
    propertyName: string,
    quantity: string,
  ) => any;
  removeIngredient?: (ingredient: IngredientModel) => any;
};

const Ingredient = ({
  ingredient,
  updateIngredient,
  removeIngredient,
}: Ingredient) => (
  <View style={styles.ingredientsContainer}>
    <View style={styles.ingredientContainer}>
      {ingredient.quantity &&
      ingredient.quantity.length > 0 &&
      !updateIngredient &&
      !removeIngredient ? (
        <Text style={styles.ingredientText}>
          {ingredient.quantity} {ingredient.unit} {ingredient.name}
        </Text>
      ) : (
        <Text style={styles.ingredientText}>{ingredient.name}</Text>
      )}

      <Image
        source={{
          uri: ingredient.image,
        }}
        style={styles.ingredientImage}
      />
    </View>

    {updateIngredient && removeIngredient && (
      <View style={styles.ingredientContainer}>
        <TextInput
          value={ingredient.quantity}
          onChangeText={quantity =>
            updateIngredient(ingredient, 'quantity', quantity)
          }
          activeUnderlineColor={Colors.primary}
          style={styles.input}
        />

        <Picker
          selectedValue={ingredient.unit}
          onValueChange={unit => updateIngredient(ingredient, 'unit', unit)}
          dropdownIconColor={Colors.primary}
          mode={'dropdown'}
          style={styles.picker}>
          {Object.keys(UnitType).map((key, index) => (
            <Picker.Item
              label={UnitType[key]}
              value={UnitType[key]}
              key={`${ingredient}-${index}`}
            />
          ))}
        </Picker>

        <Icon
          name="trash"
          size={25}
          color={Colors.red}
          onPress={() => removeIngredient(ingredient)}
        />
      </View>
    )}
  </View>
);

export default Ingredient;

const styles = StyleSheet.create({
  ingredientsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: Colors.accent,
    borderBottomWidth: 1,
    borderBottomColor: Colors.accent,
    marginBottom: 10,
  },
  ingredientContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ingredientText: {
    color: Colors.black,
    textAlignVertical: 'center',
    marginRight: 5,
    fontSize: 24,
  },
  ingredientImage: {
    height: 30,
    width: 30,
  },
  input: {
    margin: 10,
    height: 40,
    backgroundColor: Colors.white,
  },
  picker: {
    width: 100,
  },
});
