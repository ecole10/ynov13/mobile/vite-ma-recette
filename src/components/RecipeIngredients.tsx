import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Ingredient, ingredientPlaceholder} from '../models/RecipeDetails';
import Fonts from '../styles/Fonts';
import Colors from '../styles/Colors';
import Config from 'react-native-config';

type RecipeIngredients = {
  ingredients: Ingredient[];
  servings: number;
  baseServings: number;
};

const RecipeIngredients = ({
  ingredients,
  servings,
  baseServings,
}: RecipeIngredients) => {
  const [ingredientList, updateList]: [
    Ingredient[],
    (ingredients: Ingredient[]) => void,
  ] = useState([ingredientPlaceholder]);

  useEffect(() => {
    updateList(ingredients);
  }, [ingredients]);

  function getFormattedQuantity(ingredient: Ingredient): string {
    const amount =
      Math.round(
        (ingredient.measures.metric.amount / baseServings) * servings * 100,
      ) / 100;
    const isPlurals = amount >= 2;
    return `${amount} ${ingredient.measures.metric.unitShort} ${
      isPlurals ? ingredient.nameClean : ingredient.name
    }`;
  }

  return (
    <View style={styles.container}>
      {ingredientList.map((ingredient, index) => (
        <View style={styles.ingredientContainer} key={`${ingredient}-${index}`}>
          <ImageBackground
            source={{
              uri: `${
                Config.SPOONACULAR_INGREDIENT_IMAGES_URL + ingredient.image
              }`,
            }}
            style={styles.image}
            resizeMode={'contain'}
          />
          <Text style={styles.text}>{getFormattedQuantity(ingredient)}</Text>
        </View>
      ))}
    </View>
  );
};

export default RecipeIngredients;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  ingredientContainer: {
    margin: 5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 25,
    elevation: 10,
    backgroundColor: 'white',
    overflow: 'hidden',
    marginVertical: 10,
  },
  text: {
    width: 80,
    textAlign: 'center',
    fontFamily: Fonts.quicksand,
    fontSize: 14,
    color: Colors.grey,
  },
});
