import React from 'react';
import {Image, StyleSheet} from 'react-native';
import Colors from '../styles/Colors';
import Fonts from '../styles/Fonts';
import {Card as CardNative, Title} from 'react-native-paper';
import logo from '../../assets/img/logo.png';

export interface Item {
  id: number | string;
  title: string;
  image: number | string;
}

type Card = {
  item: Item;
  onPress: (item: Item) => any;
};

const Card = ({item, onPress}: Card) => (
  <CardNative elevation={5} style={styles.card} onPress={() => onPress(item)}>
    <CardNative.Cover
      source={
        typeof item.image === 'number'
          ? item.image
          : {
              uri: item.image || Image.resolveAssetSource(logo).uri,
            }
      }
      resizeMode={
        item.image || typeof item.image === 'number' ? 'cover' : 'contain'
      }
      style={styles.cardImage}
    />
    <CardNative.Content style={styles.cardContent}>
      <Title style={styles.cardTitle} numberOfLines={2}>
        {item.title}
      </Title>
    </CardNative.Content>
  </CardNative>
);

export default Card;

const styles = StyleSheet.create({
  card: {
    borderRadius: 20,
    width: 160,
    marginTop: 10,
    marginBottom: 10,
  },
  cardImage: {
    height: 120,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  cardContent: {
    height: 60,
    justifyContent: 'center',
  },
  cardTitle: {
    fontSize: 14,
    fontFamily: Fonts.quicksand,
    fontWeight: 'bold',
    color: Colors.primary,
    textAlign: 'center',
    lineHeight: 18,
  },
});
