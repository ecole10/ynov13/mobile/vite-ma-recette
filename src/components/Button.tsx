import React from 'react';
import Colors from '../styles/Colors';
import {TouchableHighlight, Text, StyleSheet} from 'react-native';
import Fonts from '../styles/Fonts';
import Icon from 'react-native-ionicons';

type Button = {
  title?: string;
  icon?: string;
  style: object;
  disabled?: boolean;
  onPress: () => any;
  iconColor?: string;
};

const Button = ({
  title,
  icon,
  style,
  disabled = false,
  onPress,
  iconColor,
}: Button) => (
  <TouchableHighlight
    onPress={onPress}
    underlayColor={Colors.accent}
    style={[styles.container, style]}
    disabled={disabled}>
    {icon ? (
      <Icon
        name={icon}
        color={iconColor ? iconColor : Colors.white}
        style={styles.icon}
      />
    ) : (
      <Text style={styles.title}>{title}</Text>
    )}
  </TouchableHighlight>
);

export default Button;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.secondary,
    display: 'flex',
    justifyContent: 'center',
  },
  icon: {
    textAlign: 'center',
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: Fonts.quicksand,
    color: Colors.white,
  },
});
