import React, {useEffect, useState} from 'react';
import {Searchbar as Search} from 'react-native-paper';
import Colors from '../styles/Colors';
import {StyleSheet} from 'react-native';

type SearchBar = {
  phrases: string[];
  icon: string;
  onIconPress: (query: string) => any;
};

const SearchBar = ({phrases, icon, onIconPress}: SearchBar) => {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [placeholder, setPlaceholder] = useState<string>('');
  let placeholderText: string = '';
  let timer: number = 0;

  const printPhrases = (
    phrases: string[],
    i: number = 0,
    phrasesIndex: number = 0,
    delay: number = 300,
  ) => {
    if (!i) {
      placeholderText = '';
      setPlaceholder(placeholderText);
    }

    const txt = phrases[phrasesIndex];

    if (i < txt.length) {
      placeholderText += txt.charAt(i);
      setPlaceholder(placeholderText);
      i++;
      timer = setTimeout(printPhrases, delay, phrases, i, phrasesIndex);
    } else {
      phrasesIndex++;
      if (phrases[phrasesIndex] === undefined) {
        timer = setTimeout(printPhrases, delay * 15, phrases);
      } else {
        i = 0;
        timer = setTimeout(printPhrases, delay * 15, phrases, i, phrasesIndex);
      }
    }
  };

  const onChangeSearch = (query: string) => setSearchQuery(query);

  useEffect(() => {
    printPhrases(phrases);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Search
      placeholder={placeholder}
      placeholderTextColor={Colors.accent}
      onChangeText={onChangeSearch}
      icon={icon}
      onIconPress={() => {
        if (searchQuery.length > 0) {
          onIconPress(searchQuery);
        }
      }}
      style={styles.search}
      iconColor={Colors.primary}
      value={searchQuery}
    />
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  search: {
    borderRadius: 20,
    marginBottom: 10,
  },
});
