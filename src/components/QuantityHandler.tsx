import {StyleSheet, TextInput, View} from 'react-native';
import React from 'react';
import Button from './Button';
import Fonts from '../styles/Fonts';
import Colors from '../styles/Colors';

type QuantityHandler = {
  value: number;
  updateValue: (value: number) => void;
  step: number;
};

const QuantityHandler = ({value, updateValue, step}: QuantityHandler) => {
  const updateQuantity = (quantity: number) => {
    if (quantity !== -1) {
      updateValue(quantity);
    }
  };

  return (
    <View style={styles.container}>
      <Button
        style={styles.button}
        onPress={() => updateQuantity(value - step)}
        title="-"
      />
      <TextInput
        onChangeText={input => updateQuantity(Number(input))}
        value={value.toString()}
        keyboardType="numeric"
        placeholder="0"
        style={styles.quantity}
      />
      <Button
        style={styles.button}
        onPress={() => updateQuantity(value + step)}
        title="+"
      />
    </View>
  );
};

export default QuantityHandler;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  quantity: {
    backgroundColor: 'white',
    textAlign: 'center',
    elevation: 10,
    fontFamily: Fonts.quicksand,
    fontSize: 24,
    color: Colors.grey,
    width: 50,
    marginHorizontal: 10,
  },
  button: {
    width: 35,
    height: 35,
    borderRadius: 20,
  },
});
