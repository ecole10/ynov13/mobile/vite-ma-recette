import React from 'react';
import Colors from '../styles/Colors';
import {StyleSheet, View} from 'react-native';
import Button from './Button';

type LineButton = {
  onPress: () => any;
};

const LineButton = ({onPress}: LineButton) => (
  <View style={styles.container}>
    <View style={styles.line} />
    <Button style={styles.button} icon="add" onPress={onPress} />
    <View style={styles.line} />
  </View>
);

export default LineButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: Colors.primary,
    borderRadius: 20,
    width: 30,
    marginLeft: 5,
    marginRight: 5,
  },
  line: {
    flex: 1,
    height: 2,
    backgroundColor: Colors.accent,
  },
});
