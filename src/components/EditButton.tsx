import React from 'react';
import Colors from '../styles/Colors';
import {StyleSheet, View} from 'react-native';
import Button from './Button';

type LineButton = {
  onPress: () => any;
};

const EditButton = ({onPress}: LineButton) => (
  <View style={styles.container}>
    <Button
      style={styles.button}
      icon="create"
      onPress={onPress}
      iconColor={Colors.primary}
    />
  </View>
);

export default EditButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 25,
    marginLeft: 5,
    backgroundColor: Colors.transparent,
  },
});
