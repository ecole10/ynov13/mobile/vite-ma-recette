import React from 'react';
import Root from './src/Root';
import {UserProvider} from './src/contexts/UserContext';
import {QueryClient, QueryClientProvider} from 'react-query';
import {setI18nConfig} from './src/utils/translation';
import {LogBox} from 'react-native';

export default function App() {
  const queryClient = new QueryClient();

  LogBox.ignoreLogs([
    "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
    "AsyncStorage has been extracted from react-native core and will be removed in a future release. It can now be installed and imported from '@react-native-async-storage/async-storage' instead of 'react-native'.",
  ]);

  setI18nConfig();

  return (
    <QueryClientProvider client={queryClient}>
      <UserProvider>
        <Root />
      </UserProvider>
    </QueryClientProvider>
  );
}
